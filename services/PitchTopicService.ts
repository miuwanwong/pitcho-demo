import { Knex } from 'knex';
import { PitchTopic } from '../models';
import { tables } from '../tables';

export class PitchTopicService {
  constructor(private knex: Knex) {}

  async getAllPitchTopics(chineseWord: string) {
    const pitchTopics = await this.knex<PitchTopic>(tables.PITCHTOPIC).where(
      'chinese_word',
      chineseWord
    );
    // const memos = await this.knex.select("*").from(tables.MEMOS);
    return pitchTopics;
  }

  async getTopic(topicID: number) {
    const foundTopic = (
      await this.knex<PitchTopic>(tables.PITCHTOPIC).select().where('pitch_topic_id', topicID)
    )[0];
    return foundTopic;
  }
}
