import { Knex } from "knex";
import { tables } from "../tables";


export class KnnService {
    constructor(private knex: Knex) { }

    async predict(data: Array<Array<Number>>) {
        return "OK!";
    }

    async getTrainingData() {
        const trainingData = await this.knex(tables.TRAINING);
        // const memos = await this.knex.select("*").from(tables.MEMOS);
        return trainingData;
    }

    async createTrainingData(data: string, label: number) {
        // console.log(data, label)
        // return 0;
        const [insertedID] = await this.knex(tables.TRAINING)
            .insert({ "data": data, "label": label })
            .returning("training_id");
        return insertedID as number;
    }
}
