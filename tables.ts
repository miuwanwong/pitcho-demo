export const tables = Object.freeze({
    TRAINING: 'training',
    REPORT: 'report',
    USERS: "users",
    VIDEOS: "videos",
    PITCHTOPIC: 'pitch_topic',

});

// export default tables;

export const trainingClass = Object.freeze({
    touchNeck: 0,
    armCrossFront: 1,
    armCrossBack: 2,
    crossLegs: 3,
    eyeglasses: 4,
    hair: 5,
    pocket: 6,
    touchForehead: 7,
    touchHand: 8,
    natual: 9,
    counting: 10,
    you: 11,
    comeTgt: 12,
    main: 13,
});


