import xlsx from "xlsx";
// import Knex from "knex";
// import * as knexConfig from "./knexfile";
import dotenv from "dotenv";

dotenv.config();


interface TrainingData { //read file果時用，所以跟返excel 入面既內容，而唔係跟db, eg. is_file ( excel係number, db係boolean), owner (excel係string, db係number)
    training_id: number;
    data: string;
    label: number;
    created_at: number;
    updated_at: number;
}

// interface CategoryData {
//     name: string;
// }
const workbook = xlsx.readFile("training.csv");

console.log(workbook.SheetNames);
export const trainingData = readSheetData<TrainingData>(workbook, "Sheet1");
// console.log(userData);
console.log(trainingData[0].label);

function readSheetData<T>(workbook: xlsx.WorkBook, sheetName: string) {
    const sheet = workbook.Sheets[sheetName];
    const data = xlsx.utils.sheet_to_json<T>(sheet);
    return data;
}

