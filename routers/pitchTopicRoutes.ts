import express from 'express';
import { pitchTopicController } from '../main';

export const pitchTopicRoutes = express.Router();

pitchTopicRoutes.get('/', pitchTopicController.getAllPitchTopics);
pitchTopicRoutes.post('/', pitchTopicController.updatePitchTopicsSession);
pitchTopicRoutes.get('/topic/:id', pitchTopicController.getTopic);
