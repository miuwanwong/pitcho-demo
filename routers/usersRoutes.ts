import express from 'express';
import { userController } from '../main';

export const userRoutes = express.Router();

userRoutes.post('/login', userController.login);
userRoutes.get('/login/google', userController.loginWithGoogle);
userRoutes.post('/users', userController.createUser);
userRoutes.get('/logout', userController.logout);
