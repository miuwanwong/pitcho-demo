import express from "express";
import { speechController } from "../main";

export const speechRoutes = express.Router();


speechRoutes.post("/paragraph", speechController.checkSimilarity); // /speech
