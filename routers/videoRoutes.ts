import express from "express";
import { videoController } from "../main";


export const videoRoutes = express.Router()

videoRoutes.post('/', videoController.createVideo);
videoRoutes.get('/', videoController.getVideo);
videoRoutes.get('/past/:id', videoController.getPastVideo);
