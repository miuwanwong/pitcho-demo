import { Request, Response } from 'express';
import { SpeechService } from '../services/SpeechService';

export class SpeechController {
  constructor(private speechService: SpeechService) {}

  checkSimilarity = async (req: Request, res: Response) => {
    try {
      const paragraph = req.body.paragraph;
      const topic = req.body.topicWord2Vec;
      const result = await this.speechService.checkSimilarity(topic, paragraph);
      res.json({ result });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };
}
