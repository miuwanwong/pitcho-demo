import { UserService } from '../services/UserService';
import { Request, Response } from 'express';
import { checkPassword } from '../hash';
import crypto from 'crypto';


export class UserController {
  constructor(private userService: UserService) {}

  login = async (req: Request, res: Response) => {
    try {
      console.log('login');
      const { emailOrUsername, password } = req.body;
      if (!emailOrUsername || !password) {
        res.status(400).json({ message: 'missing username/email/password' });
        return;
      }
      const foundUser = await this.userService.login(emailOrUsername);
      if (!foundUser || !(await checkPassword(password, foundUser.password))) {
        res.status(400).json({ message: 'invalid email/password' });
        return;
      }
      req.session['user'] = { id: foundUser.id, username: foundUser.username, email: foundUser.email };
      
      res.status(200).json({ message: 'success' });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  loginWithGoogle = async (req: Request, res: Response) => {
    try {
      const accessToken = req.session['grant'].response.access_token;
      const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
        headers: { Authorization: `Bearer ${accessToken}` },
      });
      const fetchedUser = await fetchRes.json();
      let foundUser = await this.userService.login(fetchedUser.email);

      if (!foundUser) {
        // Option 1 :幫佢開返, random 一個password
        const generatedPassword = crypto.randomBytes(20).toString('hex');
        const username = fetchedUser.email;
        const email = fetchedUser.email;
        await this.userService.createUser(username, email, generatedPassword);
      }
      req.session['user'] = { username: fetchedUser.email, email: fetchedUser.email };
      res.redirect('/home.html');
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  createUser = async (req: Request, res: Response) => {
    try {
      const { username, email, password } = req.body;
      await this.userService.createUser(username, email, password);
      res.status(200).json({ message: 'success' });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  logout = async (req: Request, res: Response) => {
    try {
      req.session.destroy(() => {
        res.redirect('/index.html');
      });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };
}
