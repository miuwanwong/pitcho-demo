import { Request, Response } from "express";
import { KnnService } from "../services/KnnService";


export class KnnController {
    constructor(private knnService: KnnService) { }

    getTrainingData = async (req: Request, res: Response) => {
        try {
            const trainingData = await this.knnService.getTrainingData();
            res.json({ trainingData });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });

        }
    };

    postTrainingData = async (req: Request, res: Response) => {
        try {
            const data = JSON.stringify(req.body.data);
            const label = req.body.label;
            if (!data) {
                res.status(400).json({ message: "invalid input" });
                return;
            }
            const ids = await this.knnService.createTrainingData(data, label)
            res.status(200).json({ message: "success", ids: ids });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });

        }
    };
}