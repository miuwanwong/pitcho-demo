import { PitchTopicService } from '../services/PitchTopicService';
// import { Server as SocketIO } from "socket.io";
import { Request, Response } from 'express';

export class PitchTopicController {
  constructor(private PitchTopicService: PitchTopicService) {}

  getAllPitchTopics = async (req: Request, res: Response) => {
    try {
      let chineseWord: any = req.query.topic;
      const PitchTopics = await this.PitchTopicService.getAllPitchTopics(chineseWord);
      res.json({ PitchTopics });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  updatePitchTopicsSession = async (req: Request, res: Response) => {
    try {
      // console.log(req.body.picthTopicID);
      const topicID = req.body.picthTopicID;
      // console.log(topicID);
      console.log('selected topicID' + ' ' + topicID);
      req.session['topicID'] = { id: topicID };
      res.json({ message: 'success save topicID' });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getTopic = async (req: Request, res: Response) => {
    try {
      const topicID = parseInt(req.params.id);
      const foundTopic = await this.PitchTopicService.getTopic(topicID);
      res.json(foundTopic);
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };
}
