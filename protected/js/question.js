displayPichTopics();

async function displayPichTopics() {
    const urlParams = new URLSearchParams(window.location.search);
    const chineseWord = urlParams.get('topic');
    console.log(chineseWord);
    const res = await fetch('/pitchTopic?topic=' + chineseWord); // default method: GET
    const data = (await res.json()).PitchTopics;
    console.log(data);
    let htmlStr = ``;
    for (let i = 0; i < data.length; i++) {
        const imgStr = data[i].picture
            ? `<img class="card-img-top questionImg" src=../photos/${data[i].picture} alt="Card image cap">`
            : ``;

        htmlStr += `<div class="question-board" onclick="getTopic2Reminder('${data[i].pitch_topic_id
            }','${data[i].topic}','${chineseWord}')">
        <form id="inner_border" class="inner_border">
            ${imgStr}
            <div class="card-body">
                <h5 class="card-title">問題 ${i + 1}</h5>
                <p class="card-text" id=${data[i].pitch_topic_id}>${data[i].topic}
                </p>
            </div>
            <span class="form-group-label">${data[i].category}</span>
        </form>
    </div>`;
    }

    const questionContainer = document.querySelector('.question-continer');
    questionContainer.innerHTML = htmlStr;
}

async function getTopic2Reminder(picthTopicID, topic, chineseWord) {
    // console.log(chineseWord)
    console.log('picthTopicID' + ' ' + picthTopicID);
    const res = await fetch('/pitchTopic', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({ picthTopicID }),
    });
    if (res.status === 200) {
        window.location.href = './pitch_detail.html?topic=' + topic + "&chineseWord =" + chineseWord;
    }
}
