const video = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementsByClassName('output_canvas')[0];
const canvasCtx = canvasElement.getContext('2d');
const landmarkContainer = document.getElementsByClassName('landmark-grid-container')[0];
const audio = document.getElementById('BGM');
const bingo = document.getElementById('bingo');
const next = document.getElementById('next');
const circeRadius = 30;
const circeRadiusH = 25;
let case_1 = true;
let case_2 = false;
let case_3 = false;
let case_4 = false;
bingo.pause();
next.pause();
audio.volume = 0.3;
audio.loop = true;
const hinX = 230;
const hinY = 450;
const hinW = 120;
const hinH = 120;
const heartImg = new Image();
heartImg.src = '../photos/fire.png';
const emojiImg = new Image();
emojiImg.src = '../photos/emoji.png';
const hinImg1 = new Image();
hinImg1.src = '../photos/case1.png';
const hinImg2 = new Image();
hinImg2.src = '../photos/case2.png';
const hinImg3 = new Image();
hinImg3.src = '../photos/case3.png';
const hinImg4 = new Image();
hinImg4.src = '../photos/case4.png';

function onResults(results) {
    if (!results.poseWorldLandmarks) {
        return;
    }
    const w = canvasElement.width;
    const h = canvasElement.height;
    // const startX = 157.5;
    // const startY = h / 1.5 - 30;
    rix = results.poseLandmarks[20].x * w;
    riy = results.poseLandmarks[20].y * h;
    lix = results.poseLandmarks[19].x * w;
    liy = results.poseLandmarks[19].y * h;
    lex = results.poseLandmarks[13].x * w;
    ley = results.poseLandmarks[13].y * h;
    rex = results.poseLandmarks[14].x * w;
    rey = results.poseLandmarks[14].y * h;
    lsx = results.poseLandmarks[11].x * w;
    lsy = results.poseLandmarks[11].y * h;
    rsx = results.poseLandmarks[12].x * w;
    rsy = results.poseLandmarks[12].y * h;
    nx = results.poseLandmarks[0].x * w;
    ny = results.poseLandmarks[0].y * h;
    // console.log(z);
    canvasCtx.save();
    canvasCtx.clearRect(0, 0, w, h);
    canvasCtx.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
    // drawConnectors(canvasCtx, results.poseLandmarks, POSE_CONNECTIONS, {
    //     color: '#00FF00',
    //     lineWidth: 4,
    // });
    // drawLandmarks(canvasCtx, results.poseLandmarks, { color: '#FF0000', lineWidth: 2 });

    // canvasCtx.fillText('填充到相應的位置', 140, 150);
    // canvasCtx.strokeRect(startX, startY, 60, 40)

    //case 1:
    if (case_1) {
        case1();
    }

    //case 2:
    if (case_2) {
        case2();
    }

    // //case 3:
    if (case_3) {
        case3();
    }

    // // case 4:
    if (case_4) {
        case4();
    }

    canvasCtx.restore();
}

const pose = new Pose({
    locateFile: (file) => {
        return `https://cdn.jsdelivr.net/npm/@mediapipe/pose/${file}`;
    },
});

pose.setOptions({
    modelComplexity: 1,
    smoothLandmarks: true,
    minDetectionConfidence: 0.5,
    minTrackingConfidence: 0.5,
});
pose.onResults(onResults);

const camera = new Camera(video, {
    onFrame: async () => {
        await pose.send({ image: video });
    },

    width: 375,
    height: 667,
});
camera.start();
video.style.display = 'none';

function drawCirce(centerX, centerY, radius, color) {
    canvasCtx.beginPath();

    canvasCtx.arc(centerX, centerY, radius, 0, 2 * Math.PI);
    canvasCtx.strokeStyle = `${color}`;
    canvasCtx.fillStyle = `${color}`;
    canvasCtx.fill();
    canvasCtx.stroke();
}

function case1() {
    const circeForRix = 188;
    const circeForRiy = 349;
    const circeForLix = 304;
    const circeForLiy = 270;
    const circeForLex = 255;
    const circeForLey = 320;
    const colorRi = 'green';
    const colorLi = 'blue';
    const colorLe = 'yellow';

    drawCirce(circeForRix, circeForRiy, circeRadius, colorRi);
    drawCirce(circeForLix, circeForLiy, circeRadius, colorLi);
    drawCirce(circeForLex, circeForLey, circeRadius, colorLe);
    drawCirce(rix, riy, circeRadiusH, colorRi);
    drawCirce(lix, liy, circeRadiusH, colorLi);
    drawCirce(lex, ley, circeRadiusH, colorLe);
    canvasCtx.font = '30px serif';
    canvasCtx.fillStyle = 'white';
    canvasCtx.fillText('提示', 150, 530);
    canvasCtx.drawImage(hinImg1, hinX, hinY, hinW, hinH);
    // canvasCtx.drawImage(heartImg, rix - 50, riy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, lix - 50, liy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, temp - 50, ley - 50, 100, 100);
    const radiusRi = calRadius(circeForRix, circeForRiy, rix, riy, circeRadius);
    const radiusLi = calRadius(circeForLix, circeForLiy, lix, liy, circeRadius);
    const radiusLe = calRadius(circeForLex, circeForLey, lex, ley, circeRadius);
    if (radiusRi || radiusLi || radiusLe) {
        // canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        audio.volume = 0.1;
        bingo.play();
    }
    if (radiusRi && radiusLi && radiusLe) {
        canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        audio.volume = 0.1;
        next.play();
        case_1 = false;
        case_2 = true;
        case_3 = false;
        case_4 = false;
    }
}

function case2() {
    const circeForRix = 71;
    const circeForRiy = 270;
    const circeForLix = 188;
    const circeForLiy = 349;
    const circeForRex = 120;
    const circeForRey = 320;
    const colorRi = 'green';
    const colorLi = 'blue';
    const colorRe = 'yellow';
    drawCirce(circeForRix, circeForRiy, circeRadius, colorRi);
    drawCirce(circeForLix, circeForLiy, circeRadius, colorLi);
    drawCirce(circeForRex, circeForRey, circeRadius, colorRe);
    drawCirce(rix, riy, circeRadiusH, colorRi);
    drawCirce(lix, liy, circeRadiusH, colorLi);
    drawCirce(rex, rey, circeRadiusH, colorRe);
    canvasCtx.font = '30px serif';
    canvasCtx.fillStyle = 'white';
    canvasCtx.fillText('提示', 150, 530);
    canvasCtx.drawImage(hinImg2, hinX, hinY, hinW, hinH);
    // canvasCtx.drawImage(heartImg, rix - 50, riy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, lix - 50, liy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, rex - 50, rey - 50, 100, 100);
    const radiusRi = calRadius(circeForRix, circeForRiy, rix, riy, circeRadius);
    const radiusLi = calRadius(circeForLix, circeForLiy, lix, liy, circeRadius);
    const radiusRe = calRadius(circeForRex, circeForRey, rex, rey, circeRadius);
    if (radiusRe || radiusLi || radiusRi) {
        // canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        audio.volume = 0.1;
        bingo.play();
    }
    if (radiusRe && radiusLi && radiusRi) {
        canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        audio.volume = 0.1;
        next.play();
        case_1 = false;
        case_2 = false;
        case_3 = true;
        case_4 = false;
    }
}

function case3() {
    const circeForRix = 71;
    const circeForRiy = 270;
    const circeForLix = 304;
    const circeForLiy = 270;
    const circeForRex = 120;
    const circeForRey = 320;
    const circeForLex = 255;
    const circeForLey = 320;
    const colorRi = 'green';
    const colorLi = 'blue';
    const colorRe = 'yellow';
    const colorLe = 'red';
    drawCirce(circeForRix, circeForRiy, circeRadius, colorRi);
    drawCirce(circeForLix, circeForLiy, circeRadius, colorLi);
    drawCirce(circeForLex, circeForLey, circeRadius, colorLe);
    drawCirce(circeForRex, circeForRey, circeRadius, colorRe);
    drawCirce(rix, riy, circeRadiusH, colorRi);
    drawCirce(lix, liy, circeRadiusH, colorLi);
    drawCirce(lex, ley, circeRadiusH, colorLe);
    drawCirce(rex, rey, circeRadiusH, colorRe);
    canvasCtx.font = '30px serif';
    canvasCtx.fillStyle = 'white';
    canvasCtx.fillText('提示', 150, 530);
    canvasCtx.drawImage(hinImg3, hinX, hinY, hinW, hinH);
    // canvasCtx.drawImage(heartImg, rix - 50, riy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, lix - 50, liy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, temp - 50, ley - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, rex - 50, rey - 50, 100, 100);
    const radiusRi = calRadius(circeForRix, circeForRiy, rix, riy, circeRadius);
    const radiusLi = calRadius(circeForLix, circeForLiy, lix, liy, circeRadius);
    const radiusRe = calRadius(circeForRex, circeForRey, rex, rey, circeRadius);
    const radiusLe = calRadius(circeForLex, circeForLey, lex, ley, circeRadius);
    if (radiusRi || radiusLi || radiusRe || radiusLe) {
        audio.volume = 0.1;
        bingo.play();
    }
    if (radiusRi && radiusLi && radiusRe && radiusLe) {
        canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        audio.volume = 0.1;
        next.play();
        case_1 = false;
        case_2 = false;
        case_3 = false;
        case_4 = true;
    }
}

function case4() {
    const circeForRex = 85;
    const circeForRey = 325;
    const circeForLex = 290;
    const circeForLey = 325;
    const circeForRsx = 120;
    const circeForRsy = 200;
    const circeForLsx = 255;
    const circeForLsy = 200;
    const circeForRix = 148;
    const circeForRiy = 349;
    const circeForLix = 228;
    const circeForLiy = 349;
    const colorRs = 'purple';
    const colorLs = 'pink';
    const colorRi = 'green';
    const colorLi = 'blue';
    // const colorRe = 'yellow';
    // const colorLe = 'red';
    drawCirce(circeForRsx, circeForRsy, circeRadius, colorRs);
    drawCirce(circeForLsx, circeForLsy, circeRadius, colorLs);
    // drawCirce(circeForLex, circeForLey, circeRadius, colorLe);
    // drawCirce(circeForRex, circeForRey, circeRadius, colorRe);
    drawCirce(circeForRix, circeForRiy, circeRadius, colorRi);
    drawCirce(circeForLix, circeForLiy, circeRadius, colorLi);
    drawCirce(rsx, rsy, circeRadiusH, colorRs);
    drawCirce(lsx, lsy, circeRadiusH, colorLs);
    drawCirce(rix, riy, circeRadiusH, colorRi);
    drawCirce(lix, liy, circeRadiusH, colorLi);
    canvasCtx.font = '30px serif';
    canvasCtx.fillStyle = 'white';
    canvasCtx.fillText('提示', 150, 530);
    canvasCtx.drawImage(hinImg4, hinX, hinY, hinW, hinH);
    // drawCirce(lex, ley, circeRadiusH, colorLe);
    // drawCirce(rex, rey, circeRadiusH, colorRe);

    // canvasCtx.drawImage(heartImg, rsx - 50, rsy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, lsx - 50, lsy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, temp - 50, ley - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, rex - 50, rey - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, rix - 50, riy - 50, 100, 100);
    // canvasCtx.drawImage(heartImg, lix - 50, liy - 50, 100, 100);
    // canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
    const radiusRs = calRadius(circeForRsx, circeForRsy, rsx, rsy, circeRadius);
    const radiusLs = calRadius(circeForLsx, circeForLsy, lsx, lsy, circeRadius);
    // const radiusRe = calRadius(circeForRex, circeForRey, rex, rey, circeRadius);
    // const radiusLe = calRadius(circeForLex, circeForLey, temp, ley, circeRadius);
    const radiusRi = calRadius(circeForRix, circeForRiy, rix, riy, circeRadius);
    const radiusLi = calRadius(circeForLix, circeForLiy, lix, liy, circeRadius);
    // const radiusLe = calRadius(circeForLex, circeForLey, lex, ley, circeRadius);
    if (radiusRs || radiusLs || radiusRi || radiusLi) {
        audio.volume = 0.1;
        bingo.play();
    }
    if (radiusRs && radiusLs && radiusRi && radiusLi) {
        audio.volume = 0.1;
        next.play();
        canvasCtx.drawImage(emojiImg, nx - 100, ny - 100, 200, 200);
        case_1 = false;
        case_2 = false;
        case_3 = false;
        case_4 = false;
    }
}

function calRadius(centerX, centerY, inputX, inputY, radius) {
    const disX = centerX - inputX;
    const disY = centerY - inputY;
    const disXPowTwo = Math.pow(disX, 2);
    const disYPowTwo = Math.pow(disY, 2);
    const dis = Math.sqrt(disXPowTwo + disYPowTwo);
    return dis < radius;
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top,
    };
}

canvasElement.addEventListener(
    'click',
    function (evt) {
        var mousePos = getMousePos(canvasElement, evt);
        alert(mousePos.x + ',' + mousePos.y);
    },
    false
);
