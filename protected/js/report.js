const videoContainer = document.getElementById('video_container');
const transcriptSection = document.getElementById('transcript_container');
const textContainer = document.getElementById('text');
const topic = document.getElementById('topic');
const analysis = document.getElementById('analysis');

const fillersContainer = document.getElementById('fillers');
const pausesBeforeClick = document.getElementById('pauses_before_click');
const pausesContainer = document.getElementById('pauses_container');
const longPauseContainer = document.getElementById('long_pause');
const shortPauseContainer = document.getElementById('short_pause');
const paceContainer = document.getElementById('pace');
const relevancyContainer = document.getElementById('relevancy');

const customLongPause = document.getElementById('custom_long_pause');
const longPauseConfirm = document.getElementById('confirm_long');

const customShortPause = document.getElementById('custom_short_pause');
const shortPauseConfirm = document.getElementById('confirm_short');

pausesBeforeClick.onclick = () => {
    pausesContainer.style.display = 'block';

    hideCustomButton();
};

function hideCustomButton() {
    customLongPause.style.display = 'none';
    longPauseConfirm.style.display = 'none';
    customShortPause.style.display = 'none';
    shortPauseConfirm.style.display = 'none';
}

let fillers = 0;
let longPauseCount = 0;
let shortPauseCount = 0;
let maxWordCount = 0;
const longPauseThreshold = 1;
const shortPauseThreshold = 0.3;

const hesitationWordMap = new Map();
hesitationWordMap.set(['唉', '誒', '哎'], 'eh');
hesitationWordMap.set(['牙', '哦'], 'uh');
hesitationWordMap.set(['嗯', '呣'], 'um');
hesitationWordMap.set(['咧', '呢', '哩'], 'leh');
hesitationWordMap.set(['咁咧', '咁呢', '咁哩'], 'gumLeh');
hesitationWordMap.set(['即系', '即係'], 'jikHai');
hesitationWordMap.set(['囉', '咯', '囖', '嚕'], 'lor');
hesitationWordMap.set(['咁嘅'], 'gumGeh');
hesitationWordMap.set(['嘅'], 'geh');
hesitationWordMap.set(['嗱'], 'laa');

const hesitationWordObj = Object.freeze({
    eh: ['唉', '誒', '哎'],
    uh: ['牙', '哦'],
    um: ['嗯', '呣'],
    leh: ['咁咧', '咁呢', '咁哩'],
    gumLeh: ['咧', '呢', '哩'],
    jikHai: ['即系', '即係'],
    lor: ['囉', '咯', '囖', '嚕'],
    gumGeh: ['咁嘅'],
    geh: ['嘅'],
    laa: ['嗱'],
});

const params = new URLSearchParams(window.location.search);
const reportID = parseInt(params.get('id'));

main();

async function main() {
    if (reportID) {
        await pastReport(reportID);
        document.body.style.overflow = 'scroll';
        videoContainer.style.position = 'relative';
    } else {
        newReport();
    }
}

async function newReport() {
    const topic = await getVideoFileName();

    video.onended = async () => {
        const res = await fetch('/report/text');
        const result = await res.json();

        showScrollIndicator();

        await processData(result, topic);
    };
}

async function getVideoFileName() {
    const res = await fetch('/video');
    const data = await res.json();
    const foundVideo = data['foundVideo'];
    const videoTopicID = data['foundVideo']['topic_id'];
    const topicWord2Vec = await getTopic(videoTopicID);
    video.innerHTML = `
<source src="../download/video/${foundVideo.file_name}.webm" type="video/webm"/>
<track src="../download/subtitle/${foundVideo.file_name}.vtt" kind="subtitles" srclang="zh" default> `;
    return topicWord2Vec;
}

async function getTopic(topicID) {
    const res = await fetch(`/pitchTopic/topic/${topicID}`);
    const foundTopic = await res.json();
    topic.innerHTML = `題目： ${foundTopic.topic}`;
    const topicWord2Vec = foundTopic['chinese_word'];
    return topicWord2Vec;
}

async function processData(result, topicWord2Vec) {
    const textDetails = result['hesitation_details'];

    let finalDetails = [];
    let tempText = [];
    let tempITN = [];
    let tempITNArray = [];

    for (const textObj of textDetails) {
        for (const wordDetail of textObj['Words']) {
            finalDetails.push(wordDetail);
        }

        for (const displayText of textObj['Display']) {
            tempText.push(displayText);
        }

        tempITN = textObj['ITN'].split(' ');
        let newTemp2 = [];
        for (const x of tempITN) {
            newTemp2.push(x);
        }
        for (const x of newTemp2) {
            tempITNArray.push(x);
        }
    }

    const finalText = tempText.join('');

    const textAppender2 = textAppendClosue(finalText);
    let finalITNArray = [];
    for (let char of tempITNArray) {
        finalITNArray = textAppender2(char);
    }

    for (let i = 0; i < finalDetails.length; i++) {
        if (finalITNArray[i] !== undefined) {
            finalDetails[i]['Word'] = finalITNArray[i];
        }
    }

    const endOfSpeechOffset = result['end_of_speech_offset'];

    textContainer.innerHTML = finalText;

    countHesitation(tempITNArray);
    showHesitation(tempITNArray, finalITNArray);

    countPauses(finalDetails, endOfSpeechOffset);
    showLongPauses(finalDetails, finalITNArray);
    showShortPauses(finalDetails, finalITNArray);
    customPauses(customLongPause, finalDetails, finalITNArray, longPauseConfirm, 'long_pause');
    customPauses(customShortPause, finalDetails, finalITNArray, shortPauseConfirm, 'short_pause');

    showPace(finalDetails, finalITNArray);

    const relevancyJSON = await countRelevancy(topicWord2Vec, finalText);
    showRelevancy(relevancyJSON, finalITNArray);
}

function textAppendClosue(finalText) {
    let result = [];
    let curIdx = 0;
    return (tempITNArrayText) => {
        let newResult = '';
        for (let i = 0; i < tempITNArrayText.length; i++) {
            if (finalText[curIdx] === tempITNArrayText[i]) {
                newResult += tempITNArrayText[i];
                curIdx = curIdx + 1;
            } else {
                newResult += tempITNArrayText[i];
                if (finalText[curIdx] !== undefined) {
                    result[result.length - 1] += finalText[curIdx];
                    curIdx = curIdx + 2;
                }
            }
        }

        result.push(newResult);
        return result;
    };
}

// ["xx", "xx", "xx", "x", "xxx"]
// ['唉', '誒', '哎']
// ['eh']

function countHesitation(tempITNArray) {
    // Getting hesitation words count
    for (let i = 0; i < tempITNArray.length; i++) {
        const wordPossibilities = hesitationWordMap.keys();
        let wordPossibility = wordPossibilities.next();

        while (!wordPossibility.done) {
            const possibleWord = wordPossibility.value;

            if (possibleWord.includes(tempITNArray[i])) {
                fillers++;
            }
            wordPossibility = wordPossibilities.next();
        }
    }
}

function showHesitation(tempITNArray, finalITNArray) {
    let textAfterAnalysisArray = [];
    for (let word of finalITNArray) {
        textAfterAnalysisArray.push(word);
    }

    // Highlight hesitation words
    fillersContainer.onclick = () => {
        hideCustomButton();

        for (let i = 0; i < tempITNArray.length; i++) {
            const wordPossibilities = hesitationWordMap.keys();
            let wordPossibility = wordPossibilities.next();

            while (!wordPossibility.done) {
                const possibleWord = wordPossibility.value;

                if (possibleWord.includes(tempITNArray[i])) {
                    if (!textAfterAnalysisArray[i].includes('hesitation')) {
                        textAfterAnalysisArray[
                            i
                        ] = `<span class='hesitation'>${textAfterAnalysisArray[i]}</span>`;
                    }
                }
                wordPossibility = wordPossibilities.next();
            }
        }

        let textAfterAnalysis = textAfterAnalysisArray.join('');
        textContainer.innerHTML = textAfterAnalysis;
        analysis.innerHTML = `猶疑用詞出現次數：${fillers}`;
    };
}

// [{ word: "xx", offset: 123, duration: 123456789 }, {}, {}]

function countPauses(finalDetails, endOfSpeechOffset) {
    // Getting long pause count & short pause count
    for (let i = 0; i < finalDetails.length; i++) {
        if (i < finalDetails.length - 1) {
            const timeGap = get_seconds(
                finalDetails[i + 1]['Offset'] -
                    (finalDetails[i]['Offset'] + finalDetails[i]['Duration'])
            );
            finalDetails[i]['Gap'] = timeGap;
        }
        if (i === finalDetails.length - 1) {
            const timeGap = get_seconds(
                endOfSpeechOffset - (finalDetails[i]['Offset'] + finalDetails[i]['Duration'])
            );
            finalDetails[i]['Gap'] = timeGap;
        }

        if (finalDetails[i]['Gap'] > longPauseThreshold) {
            longPauseCount++;
        }

        if (
            finalDetails[i]['Gap'] < longPauseThreshold &&
            finalDetails[i]['Gap'] > shortPauseThreshold
        ) {
            shortPauseCount++;
        }
        // Getting duration data
        finalDetails[i]['Duration'] = get_seconds(finalDetails[i]['Duration']);
    }
}

function showLongPauses(finalDetails, finalITNArray) {
    let textAfterAnalysisArray = [];
    for (let word of finalITNArray) {
        textAfterAnalysisArray.push(word);
    }

    // Show long_pause
    longPauseContainer.onclick = () => {
        customLongPause.style.display = 'block';
        longPauseConfirm.style.display = 'block';

        for (let i = 0; i < finalDetails.length; i++) {
            if (finalDetails[i]['Gap'] > 0.8) {
                if (!textAfterAnalysisArray[i].includes('long_pause')) {
                    textAfterAnalysisArray[i] = `${
                        textAfterAnalysisArray[i]
                    }<span class='long_pause' style="padding-right: ${
                        finalDetails[i]['Gap'] * 16
                    }px;"></span>`;
                }
            }
        }

        pausesContainer.style.display = 'none';

        let textAfterAnalysis = textAfterAnalysisArray.join('');
        textContainer.innerHTML = textAfterAnalysis;

        analysis.innerHTML = `超過0.8秒的長停頓：${longPauseCount}`;
    };
}

function customPauses(customPauseInput, finalDetails, finalITNArray, confirmButton, className) {
    confirmButton.onclick = () => {
        let textAfterAnalysisArray = [];
        for (let word of finalITNArray) {
            textAfterAnalysisArray.push(word);
        }

        let customPause = customPauseInput.value / 10;
        let customCount = 0;

        for (let i = 0; i < finalDetails.length; i++) {
            if (finalDetails[i]['Gap'] > customPause) {
                customCount++;
            }
            analysis.innerHTML = `超過${customPause}秒的停頓：${customCount}`;
        }

        for (let i = 0; i < finalDetails.length; i++) {
            if (finalDetails[i]['Gap'] > customPause) {
                if (!textAfterAnalysisArray[i].includes(className)) {
                    textAfterAnalysisArray[i] = `${
                        textAfterAnalysisArray[i]
                    }<span class=${className} style="padding-right: ${
                        finalDetails[i]['Gap'] * 16
                    }px;"></span>`;
                }
            }
        }
        let textAfterAnalysis = textAfterAnalysisArray.join('');
        textContainer.innerHTML = textAfterAnalysis;
    };
}

function showShortPauses(finalDetails, finalITNArray) {
    let textAfterAnalysisArray = [];
    for (let word of finalITNArray) {
        textAfterAnalysisArray.push(word);
    }

    // Show short_pause
    shortPauseContainer.onclick = () => {
        customShortPause.style.display = 'block';
        shortPauseConfirm.style.display = 'block';

        for (let i = 0; i < finalDetails.length; i++) {
            if (finalDetails[i]['Gap'] < 0.8 && finalDetails[i]['Gap'] > 0.3) {
                if (!textAfterAnalysisArray[i].includes('short_pause')) {
                    textAfterAnalysisArray[i] = `${
                        textAfterAnalysisArray[i]
                    }<span class='short_pause' style="padding-right: ${
                        finalDetails[i]['Gap'] * 16
                    }px;"></span>`;
                }
            }
        }
        pausesContainer.style.display = 'none';

        let textAfterAnalysis = textAfterAnalysisArray.join('');
        textContainer.innerHTML = textAfterAnalysis;

        analysis.innerHTML = `0.3-0.8秒的短停頓：${shortPauseCount}`;
    };
}

function showPace(finalDetails, finalITNArray) {
    let textAfterAnalysisArray = [];
    for (let word of finalITNArray) {
        textAfterAnalysisArray.push(word);
    }

    // Getting pace
    paceContainer.onclick = () => {
        hideCustomButton();

        for (let i = 0; i < finalDetails.length; i++) {
            let averagePace = finalDetails[i]['Duration'] / finalDetails[i]['Word'].length;
            if (averagePace <= 0.1) {
                if (!textAfterAnalysisArray[i].includes(`<span class='fast'>`)) {
                    textAfterAnalysisArray[
                        i
                    ] = `<span class='fast'>${textAfterAnalysisArray[i]}</span>`;
                }
            }
            if (averagePace >= 0.3) {
                if (!textAfterAnalysisArray[i].includes(`<span class='slow'>`)) {
                    textAfterAnalysisArray[
                        i
                    ] = `<span class='slow'>${textAfterAnalysisArray[i]}</span>`;
                }
            }
        }

        let displayText = textAfterAnalysisArray.join('');
        textContainer.innerHTML = displayText;

        analysis.innerHTML = /*html*/ `
        <div id="fast" class="fast">語速偏快 (約每分鐘600字)</div>
        <div id="slow" class="slow">語速偏慢 (約每分鐘200字)</div>
        <div id="description">廣東話的對話語速約為每分鐘160字至170字，演講語速可達每分鐘240字</div>

        `;
    };
}

async function countRelevancy(topicWord2Vec, paragraph) {
    const res = await fetch('/speech/paragraph', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({ topicWord2Vec, paragraph }),
    });
    const relevancyJSON = await res.json();

    return relevancyJSON;
}

function showRelevancy(relevancyJSON, finalITNArray) {
    let textAfterAnalysisArray = [];
    for (let word of finalITNArray) {
        textAfterAnalysisArray.push(word);
    }

    relevancyContainer.onclick = async () => {
        hideCustomButton();

        const details = relevancyJSON['result']['resultRelevant'];

        let relevantArray = [];
        let analysisString = `貼題字眼：`;

        for (let detail of details) {
            relevantArray.push(detail.word);
        }

        const string = relevantArray.join(', ');
        analysisString += string;

        for (let i = 0; i < textAfterAnalysisArray.length; i++) {
            for (let relWord of relevantArray) {
                if (textAfterAnalysisArray[i].indexOf(relWord) !== -1) {
                    textAfterAnalysisArray[
                        i
                    ] = `<span class='relevant'>${textAfterAnalysisArray[i]}</span>`;
                }
            }
        }

        let displayText = textAfterAnalysisArray.join('');
        textContainer.innerHTML = displayText;

        analysis.innerHTML = analysisString;
    };
}

async function pastReport(reportID) {
    const videoRes = await fetch(`/video/past/${reportID}`);
    const videoData = await videoRes.json();
    const videoTopicID = videoData['foundVideo']['topic_id'];
    const topicWord2Vec = await getTopic(videoTopicID);

    const res = await fetch(`/report/past_reports_single/${reportID}`);
    const report = await res.json();

    video.innerHTML = `
<source src="../download/video/${report.video_filename}.webm" type="video/webm"/>
<track src="../download/subtitle/${report.video_filename}.vtt" kind="subtitles" srclang="zh" default> `;

    await processData(report, topicWord2Vec);
}

function showScrollIndicator() {
    document.body.style.overflow = 'scroll';
    document.querySelector('.scroll').style.display = 'block';
    document.querySelector('.scroll').addEventListener('click', () => {
        document.querySelector('.scroll').style.display = 'none';
        videoContainer.style.position = 'relative';
        transcriptSection.scrollIntoView(true);
    });
}

function get_seconds(nanoseconds_100) {
    const seconds = parseFloat((nanoseconds_100 / 10000000).toFixed(2));
    return seconds;
}
