const username = document.getElementById('username');
const timeline = document.getElementById('timeline');
const cards = document.getElementById('cards');
const lines = document.getElementById('lines');
let cardsHtmlString = '';
let linesHtmlString = '';

async function showAllReports() {
    const res = await fetch('/report/past_reports');
    const results = await res.json();
    username.innerHTML = `${results[0].username} 的練習成果`;
    for (report of results) {
        cardsHtmlString += /* html*/ `
        <a href="report.html?id=${report.id}">
    <div class="card">
        <h4> ${moment(report.created_at).format('MMMM Do YYYY, h:mm a')}</h4>
        <p>${report.topic}</p>
    </div>
</a>`;
        linesHtmlString += /* html*/ `<div class="dot"></div>
    <div class="line"></div>`;
    }

    cards.innerHTML = cardsHtmlString;
    lines.innerHTML = linesHtmlString;
}

showAllReports();
