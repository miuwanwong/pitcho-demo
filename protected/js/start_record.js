const blazeCam = document.getElementsByClassName('input_video')[0]; //--blaze
const canvasElement = document.getElementsByClassName('output_canvas')[0]; //--blaze
const canvasCtx = canvasElement.getContext('2d');
const audience = document.getElementsByClassName('recording')[0];
const start = document.getElementsByClassName('start')[0];
const button = document.getElementsByClassName('confirm-button')[0];
const p = document.querySelector('#predict');
const loading = document.getElementById('loading');
const micImg = new Image();
micImg.src = '../photos/mic2.png';
const humanImg = new Image();
humanImg.src = '../photos/human2.png';
const micW = 70;
const micH = 137;
const startX = 145;
const startY = 250;
let showTimer = document.getElementById('timer');
let showCountDown = document.getElementById('count-down');
let media_recorder = null;
let camera_stream = null;
let blobs_recorded;
let videoDuration = 90;
let countDownSec = 3;
let countDownID, durationID;
let recording = false;
// let ready = false;
// let videoLandmarkArr = [];
let predictedResultArr = [];
let count = 1;
let landmarkResult;
let socket = io.connect();

blazeCam.style.display = 'none';
audience.style.display = 'none';
start.style.display = 'none';
showCountDown.style.display = 'none';
button.style.display = 'none';
p.style.display = 'none';
loading.style.display = 'none';

// window.addEventListener('onload', function () {
//     loading.style.display = 'flex';
// });

window.addEventListener('load', async function () {
    loading.style.display = 'flex';
    await setupClassifyData();

    camera_stream = await navigator.mediaDevices.getUserMedia({
        video: { width: 375, height: 600 },
        audio: true,
    });

    media_recorder = new MediaRecorder(camera_stream, {
        mimeType: 'video/webm',
    });

    // event : new recorded video blob available
    media_recorder.addEventListener('dataavailable', function (e) {
        e.preventDefault();
        blobs_recorded = e.data;

        // console.log(blobs_recorded.length);
    });

    // event : recording stopped & all blobs sent
    media_recorder.addEventListener('stop', function (e) {
        e.preventDefault();
        recording = false;
        // audience.style.display = 'none';
    });
    loading.style.display = 'none';
});
// ---------------------------------------------------------------------
// })

function onResults(results) {
    if (!results.poseWorldLandmarks) {
        // loading.style.display = 'flex';
    }
    const w = canvasElement.width;
    const h = canvasElement.height;
    const inFrame = results.poseLandmarks[27].y * h;

    const lcx = results.poseLandmarks[19].x * w;
    const lcy = results.poseLandmarks[19].y * h;
    const rcx = results.poseLandmarks[20].x * w;
    const rcy = results.poseLandmarks[20].y * h;

    lastPoseLandmarks = results.poseLandmarks;

    if (recording) {
        classify(results.poseLandmarks);
        // console.log(classify(results.poseLandmarks))
        const predicted = document.querySelector('#predict').innerHTML;
        console.log(predicted);
        predictedResultArr.push(predicted);
        // const timeStamp = new Date().getTime();
        // predictedResultArr.push({ time: timeStamp, label: `${predicted}` });
    }
    canvasCtx.save();
    canvasCtx.clearRect(0, 0, w, h);
    canvasCtx.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
    drawConnectors(canvasCtx, results.poseLandmarks, POSE_CONNECTIONS, {
        color: '#00FF00',
        lineWidth: 4,
    });
    drawLandmarks(canvasCtx, results.poseLandmarks, {
        color: '#FF0000',
        lineWidth: 2,
    });
    if (inFrame < 700 && rcx > 0 && lcx < 375) {
        ready++;
        if (ready > 30) {
            // console.log('ready to start');
            canvasCtx.drawImage(micImg, startX, startY, micW, micH);
            isStart(lcx, lcy);
            isStart(rcx, rcy);
        }
    } else {
        ready = 0;
        canvasCtx.drawImage(humanImg, 0, 50, 375, 667);
    }

    canvasCtx.restore();
}

const pose = new Pose({
    locateFile: (file) => {
        return `https://cdn.jsdelivr.net/npm/@mediapipe/pose/${file}`;
    },
});

pose.setOptions({
    modelComplexity: 1,
    smoothLandmarks: true,
    minDetectionConfidence: 0.5,
    minTrackingConfidence: 0.5,
});

pose.onResults(onResults);

const camera = new Camera(blazeCam, {
    onFrame: async () => {
        await pose.send({ image: blazeCam });
    },

    width: 375,
    height: 667,
});

camera.start();

function isStart(x, y) {
    // console.log("go")
    if (count === 1) {
        if (x > startX + 10 && x < startX + 60) {
            if (y > startY && y < startY + 85) {
                count--;
                // canvasElement.style.display = 'none';
                canvasElement.remove();
                audience.style.display = 'block';

                countDown().then(() => {
                    showCountDown.remove();
                    start.style.display = 'block';
                    media_recorder.start();

                    recording = true;
                    timeStart().then(() => {
                        media_recorder.stop();
                        start.remove();
                        button.style.display = 'flex';
                    });
                });
            }
        }
    }
}

// time = t sec

async function countDown() {
    let start = await new Promise((resolve, reject) => {
        setInterval(() => {
            showCountDown.style.display = 'block';
            showCountDown.innerHTML = `${countDownSec}`;

            if (countDownSec == 0) {
                clearInterval(this);
                resolve('start');
            }
            countDownSec--;
        }, 1000);
    });
}

async function timeStart() {
    let stop = await new Promise((resolve, reject) => {
        setInterval(() => {
            showTimer.innerHTML = `${videoDuration} SEC`;
            if (videoDuration == 0) {
                clearInterval(this);
                resolve('stop');
            }
            videoDuration--;
        }, 1000);
    });
}

async function sendVideo() {
    let sent = await new Promise((resolve, reject) => {
        socket.emit('video', blobs_recorded);
        socket.emit('subtitle', predictedResultArr);
        socket.on('saveVideo', (filename) => {
            resolve(filename);
        });
    });

    return sent;
}

document.getElementById('see-report').addEventListener('click', async function (e) {
    e.preventDefault();

    loading.style.display = 'flex';
    button.disabled = 'true';
    const videoFileName = await sendVideo();

    const res = await fetch('/video', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({ videoFileName }),
    });

    if (res.status === 200) {
        const response = await res.json(); // { message: 'success', id: insertedVideoID }
        window.location.assign('report.html');
    } else {
        const err = await res.json();
        console.log(err);
    }
});

document.getElementById('try-again').addEventListener('click', async function (e) {
    e.preventDefault();
    location.reload();
});

// ------------------------------------------------------
