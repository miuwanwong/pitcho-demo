import express from 'express';
import expressSession from 'express-session';
import path from 'path';
import Knex from 'knex';
import dotenv from 'dotenv';
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import fs from 'fs';
import { changeExtension } from './utils';
import { msToTime } from './utils';
import { isLoggedIn } from './guards';
dotenv.config();

import * as knexConfig from './knexfile';
export const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server, {
    maxHttpBufferSize: 10e8,
});


app.use(express.json({ limit: '50mb' }));
app.use(
    express.urlencoded({
        limit: '50mb',
        extended: true,
        parameterLimit: 50000,
    })
);

const sessionMiddleware = expressSession({
    secret: 'Pitcho',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});

app.use(sessionMiddleware);

io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res as express.Response;
    sessionMiddleware(req, res, next as express.NextFunction);
});


import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';

import { KnnService } from './services/KnnService';
import { KnnController } from './controllers/KnnController';

import { VideoService } from './services/VideoService';
import { VideoController } from './controllers/videoController';

import { SpeechService } from './services/SpeechService';
import { SpeechController } from './controllers/SpeechController';

import { ReportService } from './services/reportService';
import { ReportController } from './controllers/reportController';

import { PitchTopicService } from './services/PitchTopicService';
import { PitchTopicController } from './controllers/PitchTopicController';

const userService = new UserService(knex);
export const userController = new UserController(userService);

const knnService = new KnnService(knex);
export const knnController = new KnnController(knnService);

const videoService = new VideoService(knex);
export const videoController = new VideoController(videoService);

const reportService = new ReportService(knex);
export const reportController = new ReportController(reportService);

const speechService = new SpeechService(knex);
export const speechController = new SpeechController(speechService);

const pitchTopicService = new PitchTopicService(knex);
export const pitchTopicController = new PitchTopicController(pitchTopicService);

io.on('connection', function (socket) {
    console.log('connected');

    socket.on('video', async (data) => {
        try {
            // console.log(data);
            const timestamp = Date.now();
            const appenedVideoFile = `${timestamp}.webm`;
            const videoFile = path.join(__dirname, `protected/download/video/${appenedVideoFile}`);
            //------------------------------------------------------------------------
            socket.on('subtitle', (predictedResultArr) => {
                let text = 'WEBVTT\n\n';
                let cueStyle = 'line:22 position:50% align:center size:100%';
                // console.log(predictedResultArr);
                let j = 0;
                let duration = 90;
                const intFrame = predictedResultArr.length - (predictedResultArr.length % duration); //有時唔係每秒24個frame
                const fps = intFrame / duration;
                const convert = predictedResultArr.length - (predictedResultArr.length % fps);
                // const interval = convert / 30;

                for (let i = 0; i < predictedResultArr.length; i += fps) { //一秒先上一次字幕
                    // console.log(i);
                    // const startTime = ('00' + j).slice(-2);
                    // const endTime = ('00' + (j + 1)).slice(-2);
                    const startTime = msToTime(i / fps * 1000.0);
                    const endTime = msToTime(((i / fps) + 1) * 1000.0);

                    // const timeFrame=msToTime(i);
                    // const timeFrame = `00:00:${startTime}.001 --> 00:00:${endTime}.000 ${cueStyle}`; //!!!!!! hard code 只可放60個字幕 
                    const timeFrame = `${startTime} --> ${endTime} ${cueStyle}`; //!!!!!! hard code 只可放60個字幕 
                    text += `${timeFrame}\n${predictedResultArr[i]}\n\n`;
                    j++;
                    if (i == convert - fps) {
                        console.log('save');
                        const name = path.join(__dirname, `protected/download/subtitle/${timestamp}.vtt`);
                        fs.appendFile(name, text, (err) => {
                            if (err) throw err;
                            console.log('subtitle has been saved!');
                        });
                    }
                    // console.log(text)
                }
            });

            //------------------------------------------------------------------------
            await fs.promises.appendFile(videoFile, data);
            socket.emit('saveVideo', timestamp);
            console.log('video has been saved!');

            const userID = socket.request['session'].user.id;
            const topicID = socket.request['session'].topicID.id;

            const wavFile = changeExtension(videoFile, 'wav');
            speechService.formatToWav(videoFile, wavFile, timestamp, userID, topicID);
        } catch (err) {
            console.error(err);
        }
    });
});

import { userRoutes } from './routers/usersRoutes';
import { knnRoutes } from './routers/KnnRoutes';
import { videoRoutes } from './routers/videoRoutes';
import { speechRoutes } from './routers/SpeechRoutes';
import { reportRoutes } from './routers/reportRoutes';
import { pitchTopicRoutes } from './routers/pitchTopicRoutes';

app.use('/pitchTopic', pitchTopicRoutes);
app.use(userRoutes);
app.use('/speech', speechRoutes);
app.use('/knn', knnRoutes);
app.use('/video', videoRoutes);
app.use('/report', reportRoutes);
app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, '')));
app.use(isLoggedIn, express.static(path.join(__dirname, 'protected')));

app.use((req, res) => {
    res.sendFile(path.join(__dirname, 'public/404.html'));
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to port: [${PORT}]`);
});

