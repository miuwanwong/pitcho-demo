import path from 'path';

export function get_seconds(nanoseconds: number) {
    const seconds = parseFloat((nanoseconds / 10000000).toFixed(2));
    return seconds;
}

export function changeExtension(file: string, extension: string) {
    const basename = path.basename(file, path.extname(file));
    return path.join(path.dirname(file), basename + `.${extension}`);
}

export function msToTime(duration: number) {
    const milliseconds = duration % 1000;
    const seconds = Math.floor((duration / 1000) % 60);
    const minutes = Math.floor((duration / (1000 * 60)) % 60);
    const hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    const hourstr = hours < 10 ? '0' + hours : hours;
    const minutestr = minutes < 10 ? '0' + minutes : minutes;
    const secondstr = seconds < 10 ? '0' + seconds : seconds;
    const ms = ('000' + milliseconds).slice(-3);

    return hourstr + ':' + minutestr + ':' + secondstr + '.' + ms;
}
