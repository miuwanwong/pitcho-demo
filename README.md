## Pitcho Demo

### How to start
1. Run `npm install` / `yarn install`
2. Create database
3. Set up all envs
4. Start python server
```bash
cd python_word2vec
uvicorn demo:app --reload
```
5. Run `npm start` / `yarn start` at root project location to start express server
6. Go to `localhost:8080`

