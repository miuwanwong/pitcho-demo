export interface Video {
    id: number;
    file_name: string;
    video_landmark_arr: String;
}


export interface Users {
    id: number;
    username: string;
    email: string;
    password: string;
}

export interface PitchTopic {
    id: number;
    topic: string;
    category: string;
    chinese_word: string;
    picture: string;
}