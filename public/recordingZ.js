const videoElement = document.getElementsByClassName('input_video')[0];
const canvasElement = document.getElementsByClassName('output_canvas')[0];
const canvasCtx = canvasElement.getContext('2d');
const landmarkContainer = document.getElementsByClassName('landmark-grid-container')[0];
// const grid = new LandmarkGrid(landmarkContainer);
// const pose_label = JSON.parse(localStorage.getItem("pose_label_json"));
// const pose_data = JSON.parse(localStorage.getItem("pose_data_json"));

showAllReports();
async function showAllReports() {
    let dataFromDB = await getTrainingData();
    let pose_data = [];
    let pose_label = [];

    dataFromDB.trainingData.forEach(trainingTableElement => {
        pose_data.push(JSON.parse(trainingTableElement.data));
        pose_label.push(trainingTableElement.label);
    });
    // console.log(pose_data)
    // console.log(pose_label)


    const classifier = knnClassifier.create();
    if (pose_label != null)
        for (let i = 0; i < pose_label.length; i++) {
            let label = pose_label[i];
            let data = [];
            for (let d of pose_data[i]) {
                data.push(d.x);
                data.push(d.y);
                data.push(d.z);
                data.push(d.visibility);
            }
            data = tf.tensor(data);
            data.reshape([-1])
            // console.log(pose_data[i])
            classifier.addExample(data, label);
        }

    let poseMarked = false;
    let lastPoseLandmarks;
    function onResults(results) {
        if (!results.poseLandmarks) {
            // grid.updateLandmarks([]);
            return;
        }
        if (!poseMarked) {
            poseMarked = true;
            console.log(results.poseLandmarks)
        }
        lastPoseLandmarks = results.poseLandmarks;
        console.log(lastPoseLandmarks)
        classify(); //translate 佢做1D Array再轉tensor

        canvasCtx.save();
        canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
        canvasCtx.drawImage(
            results.image, 0, 0, canvasElement.width, canvasElement.height);
        drawConnectors(canvasCtx, results.poseLandmarks, POSE_CONNECTIONS,
            { color: '#00FF00', lineWidth: 4 });
        drawLandmarks(canvasCtx, results.poseLandmarks,
            { color: '#FF0000', lineWidth: 2 });
        canvasCtx.restore();

        // grid.updateLandmarks(results.poseWorldLandmarks);
    }

    const pose = new Pose({
        locateFile: (file) => {
            return `https://cdn.jsdelivr.net/npm/@mediapipe/pose@0.4.1624666670/${file}`;
        }
    });
    pose.setOptions({
        modelComplexity: 1,
        smoothLandmarks: true,
        minDetectionConfidence: 0.5,
        minTrackingConfidence: 0.5
    });
    pose.onResults(onResults);

    const camera = new Camera(videoElement, {
        onFrame: async () => {
            await pose.send({ image: videoElement });
        },
        width: 375,
        height: 667,
        facingMode: "environment"
    });
    camera.start();

    async function classify() {
        let data = [];
        for (let d of lastPoseLandmarks) {
            data.push(d.x);
            data.push(d.y);
            data.push(d.z);
            data.push(d.visibility);
        }
        data = tf.tensor(data);
        data.reshape([-1]);
        const res = await classifier.predictClass(data, 3);
        // console.log(res)
        resArray = Object.entries(res.confidences);
        // console.log("entire")
        // console.log(resArray);

        // let predictedClassWithConfidences = 0;

        // for (let i = 0; i < resArray.length; i++) {//Object.entries(res) 係將佢變成2d array[[key,array], [key,array]]
        //     if (resArray[i][1] > 0.5) {
        //         console.log("resArray[i][0]")
        //         console.log(resArray[i][0])
        //         predictedClassWithConfidences = resArray[i][0]
        //     }
        // }


        // console.log(pose_label)
        const trainingClass = Object.freeze({
            0: "touch Neck Left", //A 要手踭要開D
            1: "touch Neck Right", //A 
            2: "arm Cross Front", //B
            3: "arm Cross Back",
            4: "cross Legs Left", // del? 88
            5: "cross Legs Right", // del? 88
            6: "eyeglasses Left", //A
            7: "eyeglasses Right", //A
            8: "hair Left", //A
            9: "hair Right", //A
            10: "pocket Both", // del? 88
            11: "touch Forehead Left", //A要手指放上D
            12: "touch Forehead Right", //A
            13: "touchHand left touch right", //B
            14: "touchHand right touch left", //B
            15: "natual",
            16: "counting Left", // del? 88
            17: "counting Right", // del? 88
            18: "you left", //狂錯 88
            19: "you right",//狂錯 88
            20: "you both",//狂錯 88
            21: "main", //88
            100: ":( 摸頭/面/頸 ＝ 可能感到不安", // 0, 1, 6, 7, 8, 9
            101: ":( 叉手/摸手 ＝ 防衛性動作，可能想拉遠和別人的距離", // 2, 3, 13, 14
            102: "^.^嘗試用手部動作配合演講，以清楚表達",//"嘗試用手部動作配合演講 ＝ Good", // 16, 17, 18, 19, 20, 21
            103: "",
            104: ":( 手撫額頭 ＝ 可能感到掙扎或懊悔" //11, 12
        });
        let gpPredictedClass;
        if (parseInt(res.label) == 0 || parseInt(res.label) == 1 || parseInt(res.label) == 6 || parseInt(res.label) == 7 || parseInt(res.label) == 8 || parseInt(res.label) == 9) {
            gpPredictedClass = 100;
        } else if (parseInt(res.label) == 2 || parseInt(res.label) == 3 || parseInt(res.label) == 13 || parseInt(res.label) == 14) {
            gpPredictedClass = 101;
        } else if (parseInt(res.label) == 21) {
            gpPredictedClass = 102;
        } else if ((parseInt(res.label) == 11 || (parseInt(res.label) == 12))) {
            gpPredictedClass = 104;
        } else gpPredictedClass = 103;

        // document.querySelector("#predict").innerHTML = `${trainingClass[res.label]}`;
        document.querySelector("#predict").innerHTML = `${trainingClass[gpPredictedClass]}`;
        // document.querySelector("#predict").innerHTML = `${trainingClass[parseInt(predictedClassWithConfidences)]}`;
    }
    // console.log(videoElement)
    console.log(document.querySelector("#save_pose"))
    document.querySelector("#save_pose").addEventListener("click", async () => {
        let pose_label = document.querySelector("#label").value;
        let pose_data = lastPoseLandmarks;

        console.log(pose_label)
        console.log(pose_data)

        let res = await postTrainingData(pose_data, pose_label);
        console.log(res.message)
    })
    // document.querySelector("#save_pose").addEventListener("click", () => {
    //     let pose_label = JSON.parse(localStorage.getItem("pose_label_json"));
    //     let pose_data = JSON.parse(localStorage.getItem("pose_data_json"));
    //     if (pose_label == null) pose_label = [];
    //     if (pose_data == null) pose_data = [];
    //     pose_label.push((new Date()).toUTCString())
    //     pose_data.push(lastPoseLandmarks)
    //     localStorage.setItem("pose_data_json", JSON.stringify(pose_data));
    //     localStorage.setItem("pose_label_json", JSON.stringify(pose_label));

    // })
}

async function getTrainingData() {

    const res = await fetch("/knn", {
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
    });
    if (res.status === 200) {

        let data = await res.json()
        console.log(data)
        return data;

    } else {

        let err = await res.json()
        console.log(err)
    }
}
async function postTrainingData(data, label) {
    let json_str = JSON.stringify({ data: data, label: label }) //????????入落去database果到要呢到stringify 1次，先fetch到過去KnnController.ts，因為過去果時會經main.ts的app.use(express.json());，去到就變左個obj，要係果邊個stringify1次先可以match返db TEXT 個type
    const res = await fetch("/knn", {
        method: "post",
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: json_str
    });
    if (res.status === 200) {
        let data = await res.json()
        console.log(data)
        return data;

    } else {
        // error handling
        let err = await res.json()
        console.log(err)
    }
}

