const login = document.getElementById('login');
const create = document.getElementById('create');
const signInForm = document.getElementById('login-form');
const signUpForm = document.getElementById('create-acount');
const signUpButton = document.getElementById('sign-up');
const signInButton = document.getElementById('sign-in');

create.style.display = 'none';
signInButton.style.display = 'none';
signUpButton.addEventListener('click', () => {
    // console.log('signUp');
    login.style.display = 'none';
    create.style.display = 'block';
    signInButton.style.display = 'block';
    signUpButton.style.display = 'none';
});
signInButton.addEventListener('click', () => {
    // console.log('signIn');
    login.style.display = 'block';
    create.style.display = 'none';
    signInButton.style.display = 'none';
    signUpButton.style.display = 'block';
});

signInForm.addEventListener('submit', async function (e) {
    e.preventDefault();
    const form = this;
    // console.log(form);
    const emailOrUsername = form['username'].value;
    const password = form['psw'].value;
    const formObject = { emailOrUsername, password };
    console.log(emailOrUsername, password);
    const res = await fetch('/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(formObject),
    });
    if (res.status === 200) {
        console.log('login success!');
        window.location = '/html/home.html';
    } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage);
    }
});

signUpForm.addEventListener('submit', async function (e) {
    e.preventDefault();
    const form = this;
    const username = form['username'].value;
    const email = form['email'].value;
    const password = form['psw'].value;
    const formObject = { username, email, password };
    const res = await fetch('/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(formObject),
    });
    if (res.status === 200) {
        console.log('signUp success!');
        signInButton.click();
        // window.location = '/home.html';
    } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage);
    }
});
