import { Knex } from 'knex';
import { tables } from '../tables';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.USERS, (table) => {
    table.string('email').notNullable;
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.USERS, (table) => {
    table.dropColumn('email');
  });
}
