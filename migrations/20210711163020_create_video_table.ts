import { Knex } from 'knex';

const videosTableName = 'videos';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(videosTableName, (table) => {
    table.increments();
    table.string("file_name").unique().notNullable();
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(videosTableName);
}

