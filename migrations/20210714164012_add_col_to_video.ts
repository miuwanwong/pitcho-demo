import { Knex } from 'knex';
import { tables } from '../tables';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.VIDEOS, (table) => {
    table.integer('topic_id').unsigned();
    table.foreign('topic_id').references('pitch_topic_id').inTable('pitch_topic');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.dropColumn('topic_id');
  });
}
