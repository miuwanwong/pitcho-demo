import { Knex } from "knex";
import { tables } from "../tables";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.integer('user_id').unsigned();
    table.foreign('user_id').references('id').inTable('users');
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.dropColumn('user_id');
  })
}

