import { Knex } from 'knex';
import { tables } from '../tables';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.VIDEOS, (table) => {
    table.string('video_landmark_arr');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tables.REPORT, (table) => {
    table.dropColumn('video_landmark_arr');
  });
}
